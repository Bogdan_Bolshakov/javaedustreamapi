package main;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        //firstTenNumbersMultiplesOfTwentySeven();
        //sortStringArrayByLength();
        //String[] myArr = new String[] {"Bayer", "Barcelona", "FCB", "PSG", "Zenit", "Liverpool", "PSV", "Real Madrid", "Real Sociedad"};
        //Predicate<String> pred1 = s -> s.length() > 3;
        //Predicate<String> pred2 = s -> s.contains("Real");
        //List<String> list1 = sortStringArrayByPredicate(myArr, pred1);
        //List<String> list2 = sortStringArrayByPredicate(myArr, pred2);
        //System.out.println(list1);
        //System.out.println(list2);
        //System.out.println(specialMapFromStringArray(myArr));
    }

    static List<String> sortStringArrayByPredicate(String[] stringArray, Predicate<String> userPredicate) {
        return Arrays.stream(stringArray).
                filter(userPredicate).
                collect(Collectors.toList());
    }

    static void firstTenNumbersMultiplesOfTwentySeven() {
        Stream<Integer> numbers = Stream.iterate(1, n -> n + 1);
        numbers.filter(n -> n % 27 == 0).
                limit(10).
                forEach(System.out::println);
    }

    static void sortStringArrayByLength() {
        String[] stringArray = new String[]{"Tommy", "Bob", "Jo", "Sam", "Alexandr"};
        Arrays.stream(stringArray).
                sorted(Comparator.comparingInt(String::length)).
                forEach(System.out::println);
    }

    static Map<Integer, Integer> specialMapFromStringArray(String[] myArr) {
        Map<Integer, Integer> myMap = new TreeMap<>();
        Arrays.stream(myArr).forEach(word -> myMap.put(word.length(),
                (int) Arrays.stream(myArr).filter(s -> s.length() == word.length()).count()));
        return myMap;
    }
}
